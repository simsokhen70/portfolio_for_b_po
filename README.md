<div align="center">
  <img  src="https://media.giphy.com/media/CgCaB0B0zYp4kVhNPf/giphy.gif" width="123">
</div>

## Project Overview

WebDev Project represents the culmination of our learning journey, combining various concepts and technologies to create a practical and valuable web application.

- _Course_: Web Development Full Stack

- _Lecturer_: Koeun Khouch

## Technologies

<div align="left">
  <img src="https://skillicons.dev/icons?i=docker" height="40" alt="docker logo"  />
  <img src="https://skillicons.dev/icons?i=aws" height="40" alt="amazonwebservices logo"  />
  <img src="https://skillicons.dev/icons?i=cloudflare" height="40" alt="cloudflare logo"  />
  <img src="https://cdn.simpleicons.org/digitalocean/0080FF" height="40" alt="digitalocean logo"  />
  <img src="https://skillicons.dev/icons?i=linux" height="40" alt="linux logo"  />
  <img src="https://skillicons.dev/icons?i=git" height="40" alt="git logo"  />
  <img src="https://skillicons.dev/icons?i=gitlab" height="40" alt="gitlab logo"  />
</div>

###

## Contributed

If you'd like to contribute to this project, please check out our GitHub repository for guidelines on how to contribute.

```ruby
git clone https://gitlab.com/dara.veasna/webstie
```

## Contact

If you have any questions, feedback, or concerns, feel free to reach out to me at [dara.veasna@dev.com](daraa.veasna@gmail.com)

# License

WebDev is released under the [MIT](https://opensource.org/license/mit/) License. You are free to use, modify, and distribute this software following the terms of the license.

<!-- Footer -->
<img src="https://raw.githubusercontent.com/trinib/trinib/82213791fa9ff58d3ca768ddd6de2489ec23ffca/images/footer.svg" width="100%">
